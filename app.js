const username = 'user';
const password = 'pass';

const auth = require('basic-auth');
const express = require('express');
const multer = require('multer');
const path = require('path');
const ffmpeg = require('fluent-ffmpeg');
const fs = require('fs');

const app = express();
const storage = multer.diskStorage({
  destination: 'uploads/',
  filename: (req, file, cb) => {
    const originalName = file.originalname;
    cb(null, originalName);
  },
});
const upload = multer({ storage: storage });


function authenticate(req, res, next) {
  const user = auth(req);
  if (!user || user.name !== username || user.pass !== password) {
    res.set('WWW-Authenticate', 'Basic realm="Authentication Required"');
    res.status(401).send('Authentication Required');
    return;
  }
  next();
}

// Serve static files from the "public" directory
app.use(authenticate, express.static('public'));

// Set up a route to handle file uploads
// app.post('/upload', authenticate, upload.single('file'), (req, res) => {
app.post('/upload', upload.single('file'), (req, res) => {
  // req.file contains information about the uploaded file
  const inputFile = req.file.path;
  const originalFilename = req.file.originalname;
  const outputFilename = path.parse(originalFilename).name + '.mp3';
  const outputPath = path.join(__dirname, 'uploads', outputFilename);

  // Use fluent-ffmpeg to extract audio
  ffmpeg(inputFile)
    .output(outputPath)
    .outputOptions(['-map 0', '-codec copy', '-c:a copy'])
    .noVideo()
    .on('end', () => {
      console.log('Audio extracted successfully.');
      res.download(outputPath, () => {
        // Clean up the temporary file
        fs.unlinkSync(inputFile);
        fs.unlinkSync(outputPath);
      });
    })
    .on('error', (err) => {
      console.error('Error extracting audio:', err);
      res.status(500).send('Error extracting audio.');
    })
    .run();
});

// Start the server
const port = 8080;
app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});
